﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace МакетКурсовойВП
{
    public partial class Registration : Form
    {
        public Registration()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            RegistrationClient rc = new RegistrationClient();
            rc.Show();
           

        }

        private void button2_Click(object sender, EventArgs e)
        {
            RegistrationEmployer re = new RegistrationEmployer();
            re.Show();
           
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Close();
            Form1 f = new Form1();
            f.Show();
        }
    }
}
