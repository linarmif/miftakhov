﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace МакетКурсовойВП
{
    public partial class Form7 : Form
    {
        public centrzEF db = new centrzEF();
        public List<employer> em;
        public Form7()
        {
            InitializeComponent();
            em = (from e in db.employer select e).ToList();
            var query = (from e in em
                         select new
                         {
                            e.num_employer, e.name_e, e.address_e, e.tel_e, e.login_e, e.pass_e
                         }).ToList();
            dataGridView1.DataSource = query;
            dataGridView1.Columns[0].HeaderText = "Номер работодателя";
            dataGridView1.Columns[1].HeaderText = "Название";
            dataGridView1.Columns[2].HeaderText = "Адрес";
            dataGridView1.Columns[3].HeaderText = "Телефон";
            dataGridView1.Columns[4].HeaderText = "Логин";
            dataGridView1.Columns[5].HeaderText = "Пароль";
          
        }

        private void Form7_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            FormAddEmployer fec = new FormAddEmployer();
            fec.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Close();
            Form7 f3 = new Form7();
            f3.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                var rows = from o in db.employer
                           where o.num_employer.ToString() == textBox1.Text
                           select o;

                foreach (var row in rows)
                {
                    db.employer.Remove(row);
                }
                db.SaveChanges();
            }
            catch { MessageBox.Show("Ошибка"); }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Close();
        }

        
    }
}
