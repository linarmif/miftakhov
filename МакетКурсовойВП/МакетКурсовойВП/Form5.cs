﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace МакетКурсовойВП
{
    public partial class Form5 : Form
    {
        public centrzEF db = new centrzEF();
        public List<vacancies> vacan;
        public Form5()
        {
            this.FormClosing += Form1_FormClosing;

            InitializeComponent();
            vacan = (from v in db.vacancies select v).ToList();
            var query = (from v in vacan
                         join e in db.employer on v.num_employer equals e.num_employer
                         select new { v.specialty, v.education, v.tstazh, e.name_e, e.tel_e }).ToList();
            dataGridView1.DataSource = query;
            dataGridView1.ReadOnly = true;
            

            dataGridView1.Columns[0].HeaderText = "Специальность";
            dataGridView1.Columns[1].HeaderText = "Образование";
            dataGridView1.Columns[2].HeaderText = "Стаж";
            dataGridView1.Columns[3].HeaderText = "Работодатель";
            dataGridView1.Columns[4].HeaderText = "Телефон";
            
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
        private bool Spec(string userName)

        {
            var spec = (from s in db.vacancies where s.specialty == textBox1.Text select s);
            if (spec.Any())
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                var query = (from v in vacan
                             join em in db.employer on v.num_employer equals em.num_employer
                             select new { v.specialty, v.education, v.tstazh, em.name_e, em.tel_e }).ToList();
                if (textBox1.Text != "")
                {

                    dataGridView1.DataSource = query.Where(p => p.specialty.ToString() == textBox1.Text.ToString()).ToList();

                }


                if (dataGridView1.RowCount == 0) MessageBox.Show("Ничего не найдено!", "Внимание");
            }
            catch { MessageBox.Show("Ошибка"); }


        }
    

            private void Form5_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "centrzDataSet.vacancies". При необходимости она может быть перемещена или удалена.
            this.vacanciesTableAdapter.Fill(this.centrzDataSet.vacancies);

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show("Вы действительно хотите закрыть?", "Внимание", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.No)
                e.Cancel = true;
            else
                e.Cancel = false;

            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var query = (from v in vacan
                         join em in db.employer on v.num_employer equals em.num_employer
                         select new { v.specialty, v.education, v.tstazh, em.name_e, em.tel_e }).ToList();
            dataGridView1.DataSource = query;
            dataGridView1.Update();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            cResume cr = new cResume();
            cr.Show();
        }
    }
}
