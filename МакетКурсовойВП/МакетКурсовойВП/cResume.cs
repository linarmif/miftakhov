﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace МакетКурсовойВП
{
    public partial class cResume : Form
    {
        public centrzEF db = new centrzEF();
        public cResume()
        {
            
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                var nresume = db.resume.Max(r => r.num_res) + 1;
                resume resumee = new resume
                {
                    num_res = nresume,
                    num_client = int.Parse(textBox1.Text),
                    specialty_c = textBox2.Text.Replace(" ", ""),
                    education_c = textBox3.Text.Replace(" ", ""),
                    cstazh = textBox4.Text.Replace(" ", ""),

                };
                db.resume.Add(resumee);
                db.SaveChanges();
                this.Close();
            }
            catch { MessageBox.Show("Ошибка"); }
        }

        private void cResume_Load(object sender, EventArgs e)
        {

        }
    }
}
