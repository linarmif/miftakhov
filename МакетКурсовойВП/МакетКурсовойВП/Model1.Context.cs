﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан по шаблону.
//
//     Изменения, вносимые в этот файл вручную, могут привести к непредвиденной работе приложения.
//     Изменения, вносимые в этот файл вручную, будут перезаписаны при повторном создании кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace МакетКурсовойВП
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class centrzEF : DbContext
    {
        public centrzEF()
            : base("name=centrzEF")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<clients> clients { get; set; }
        public virtual DbSet<employer> employer { get; set; }
        public virtual DbSet<resume> resume { get; set; }
        public virtual DbSet<vacancies> vacancies { get; set; }
        public virtual DbSet<dogovor> dogovor { get; set; }
    }
}
