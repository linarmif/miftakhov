﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace МакетКурсовойВП
{
    public partial class RegistrationEmployer : Form
    {
        public centrzEF db = new centrzEF();
        public RegistrationEmployer()
        {
            InitializeComponent();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                var nemployer = db.employer.Max(r => r.num_employer) + 1;
                employer employerr = new employer
                {
                    num_employer = nemployer,
                    name_e = textBox6.Text.Replace(" ", ""),
                    address_e = textBox7.Text.Replace(" ", ""),
                    tel_e = textBox8.Text.Replace(" ", ""),
                    login_e = textBox9.Text.Replace(" ", ""),
                    pass_e = textBox10.Text.Replace(" ", ""),

                };
                db.employer.Add(employerr);
                db.SaveChanges();
                this.Close();
                Form1 f = new Form1();
                f.Show();
            }
            catch { MessageBox.Show("Ошибка"); }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
