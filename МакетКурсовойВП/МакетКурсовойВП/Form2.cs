﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace МакетКурсовойВП
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            this.FormClosing += Form1_FormClosing;
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form3 f3 = new Form3();
            f3.Show();
            
        }
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show("Вы действительно хотите закрыть?", "Внимание", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.No)
                e.Cancel = true;
            else
                e.Cancel = false;


        }
        private void button4_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form7 f = new Form7();
            f.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            aResume far = new aResume();
            far.Show();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            aVacancies av = new aVacancies();
            av.Show();
        }
    }
}
