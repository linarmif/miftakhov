﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace МакетКурсовойВП
{
    public partial class RegistrationClient : Form
    {
        public centrzEF db = new centrzEF();
        public RegistrationClient()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                DateTime registrationDate = DateTime.Now;
                var nclient = db.clients.Max(r => r.num_client) + 1;
                clients clientt = new clients
                {
                    num_client = nclient,
                    surname_c = textBox1.Text.Replace(" ", ""),
                    name_c = textBox2.Text.Replace(" ", ""),
                    otchestvo_c = textBox3.Text.Replace(" ", ""),
                    address_c = textBox4.Text.Replace(" ", ""),
                    tel_c = textBox5.Text.Replace(" ", ""),
                    date_uch = registrationDate,
                    date_rozhd = DateTime.Parse(textBox6.Text),
                    login_c = textBox7.Text.Replace(" ", ""),
                    pass_c = textBox8.Text.Replace(" ", "")
                };
                db.clients.Add(clientt);
                db.SaveChanges();
                this.Close();
                Form1 f = new Form1();
                f.Show();
            }
            catch { MessageBox.Show("Ошибка"); }


        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
