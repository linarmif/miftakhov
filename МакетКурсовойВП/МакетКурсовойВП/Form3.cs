﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace МакетКурсовойВП
{
    public partial class Form3 : Form
    {
        public centrzEF db = new centrzEF();
        public List<clients> cl;
        public Form3()
        {
            
            InitializeComponent();
            cl = (from c in db.clients select c).ToList();
            var query = (from c in cl
                         select new
                         {
                             c.num_client,
                             c.surname_c,
                             c.name_c,
                             c.otchestvo_c,
                             c.address_c,
                             c.tel_c,
                             c.date_uch,
                             c.date_rozhd,
                             c.login_c,
                             c.pass_c
                         }).ToList();
            dataGridView1.DataSource = query;
            dataGridView1.Columns[0].HeaderText = "Номер клиента";
            dataGridView1.Columns[1].HeaderText = "Фамилия";
            dataGridView1.Columns[2].HeaderText = "Имя";
            dataGridView1.Columns[3].HeaderText = "Отчество";
            dataGridView1.Columns[4].HeaderText = "Адрес";
            dataGridView1.Columns[5].HeaderText = "Телефон";
            dataGridView1.Columns[6].HeaderText = "Дата постановки на учет";
            dataGridView1.Columns[7].HeaderText = "Дата рождения";
            dataGridView1.Columns[8].HeaderText = "Логин";
            dataGridView1.Columns[9].HeaderText = "Пароль";
        }

        

        private void button1_Click(object sender, EventArgs e)
        {
            FormAddClients fc = new FormAddClients();
            fc.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Close();
            Form3 f3 = new Form3();
            f3.Show();

        }
        
        private void button4_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            try
            {
                var rows = from o in db.clients
                           where o.num_client.ToString() == textBox1.Text
                           select o;

                foreach (var row in rows)
                {
                    db.clients.Remove(row);
                    MessageBox.Show("Удалено");
                }
                db.SaveChanges();
            }
            catch { MessageBox.Show("Ошибка"); }
        }

        private void button4_Click_1(object sender, EventArgs e)
        {
            Close();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            
        }

        private void Form3_Load(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
