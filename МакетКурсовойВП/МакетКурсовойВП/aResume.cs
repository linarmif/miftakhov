﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace МакетКурсовойВП
{
    public partial class aResume : Form
    {
        public centrzEF db = new centrzEF();
        public List<resume> re;
        public aResume()
        {
            InitializeComponent();
            re = (from c in db.resume select c).ToList();
            var query = (from r in re
                         select new
                         {
                             r.num_res,
                             r.num_client,
                             r.specialty_c,
                             r.education_c,
                             r.cstazh,
                         }).ToList();
            dataGridView1.DataSource = query;
            dataGridView1.Columns[0].HeaderText = "Номер резюме";
            dataGridView1.Columns[1].HeaderText = "Номер клиента";
            dataGridView1.Columns[2].HeaderText = "Специальность";
            dataGridView1.Columns[3].HeaderText = "Образование";
            dataGridView1.Columns[4].HeaderText = "Стаж";
           
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        

        private void button3_Click(object sender, EventArgs e)
        {
            Close();
            aResume f3 = new aResume();
            f3.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                var rows = from o in db.resume
                           where o.num_res.ToString() == textBox1.Text
                           select o;

                foreach (var row in rows)
                {
                    db.resume.Remove(row);
                }
                db.SaveChanges();
            }
            catch { MessageBox.Show("Ошибка"); }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
