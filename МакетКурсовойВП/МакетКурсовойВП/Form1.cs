﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace МакетКурсовойВП
{
    public partial class Form1 : Form
    {
        public centrzEF db = new centrzEF();
        public Form1()
        {
            
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }
        private bool IsvalidClient(string userName, string password)

        {
            var logpas_c = (from lc in db.clients where lc.login_c == textBox1.Text && lc.pass_c == textBox2.Text select lc);
            if (logpas_c.Any())
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private bool IsvalidEmployer(string userName, string password)

        {
            var logpas_e = (from le in db.employer where le.login_e == textBox1.Text && le.pass_e == textBox2.Text select le);
            if (logpas_e.Any())
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (IsvalidClient(textBox1.Text, textBox2.Text))
                {
                    Form5 F = new Form5();
                    F.Show();
                    this.Hide();

                }
                else if (IsvalidEmployer(textBox1.Text, textBox2.Text))
                {
                    Form6 F = new Form6();
                    F.Show();
                    this.Hide();
                }
                else if (textBox1.Text == "admin" && textBox2.Text == "password")
                {
                    Form2 F = new Form2();
                    F.Show();
                    this.Hide();
                }
                else { MessageBox.Show("Неправильный логин или пароль!"); textBox1.Text = ""; textBox2.Text = ""; }
            }
            catch { MessageBox.Show("Ошибка"); }

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Registration r = new Registration();
            r.Show();
            this.Hide();
        }
    }
}
