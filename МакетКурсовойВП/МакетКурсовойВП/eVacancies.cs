﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace МакетКурсовойВП
{
    public partial class eVacancies : Form
    {
        public centrzEF db = new centrzEF();
        public eVacancies()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                var nvacancies = db.vacancies.Max(r => r.num_vac) + 1;
                vacancies vacanciess = new vacancies
                {
                    num_vac = nvacancies,
                    num_employer = int.Parse(textBox1.Text),
                    specialty = textBox2.Text.Replace(" ", ""),
                    education = textBox3.Text.Replace(" ", ""),
                    tstazh = textBox4.Text.Replace(" ", ""),

                };
                db.vacancies.Add(vacanciess);
                db.SaveChanges();
                this.Close();
            }
            catch { MessageBox.Show("Ошибка"); }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
