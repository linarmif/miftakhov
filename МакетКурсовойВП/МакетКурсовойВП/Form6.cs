﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace МакетКурсовойВП
{
    public partial class Form6 : Form
    {
        public centrzEF db = new centrzEF();
        public List<resume> res;
        public Form6()
        {
            this.FormClosing += Form1_FormClosing;

            InitializeComponent();

            res = (from r in db.resume select r).ToList();
            var query = (from r in res
                         join c in db.clients on r.num_client equals c.num_client
                         select new {r.specialty_c, r.education_c, r.cstazh, c.surname_c, c.name_c, 
                             c.otchestvo_c, c.date_rozhd, c.tel_c }).ToList();
            dataGridView1.DataSource = query;
            dataGridView1.ReadOnly = true;
            dataGridView1.Columns[0].HeaderText = "Специальность";
            dataGridView1.Columns[1].HeaderText = "Образование";
            dataGridView1.Columns[2].HeaderText = "Стаж";
            dataGridView1.Columns[3].HeaderText = "Фамилия";
            dataGridView1.Columns[4].HeaderText = "Имя";
            dataGridView1.Columns[5].HeaderText = "Отчество";
            dataGridView1.Columns[6].HeaderText = "Дата рождения";
            dataGridView1.Columns[7].HeaderText = "Телефон";
            
        }
        

        private void Form6_Load(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {

            var query = (from r in res
                         join c in db.clients on r.num_client equals c.num_client
                         select new { r.specialty_c, r.education_c, r.cstazh, c.surname_c, c.name_c, c.otchestvo_c, c.date_rozhd, c.tel_c }
                          ).ToList();
            dataGridView1.DataSource = query;
            dataGridView1.Update();
        }
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show("Вы действительно хотите закрыть?", "Внимание", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.No)
                e.Cancel = true;
            else
                e.Cancel = false;


        }

        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                var query = (from r in res
                             join c in db.clients on r.num_client equals c.num_client
                             select new { r.specialty_c, r.education_c, r.cstazh, c.surname_c, c.name_c, c.otchestvo_c, c.date_rozhd, c.tel_c }
                              ).ToList();
                if (textBox1.Text != "")
                {

                    dataGridView1.DataSource = query.Where(p => p.specialty_c.ToString() == textBox1.Text.ToString()).ToList();

                }


                if (dataGridView1.RowCount == 0) MessageBox.Show("Ничего не найдено!", "Внимание");
            }
            catch { MessageBox.Show("Ошибка"); }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            eVacancies ev = new eVacancies();
            ev.Show();
        }
    }
}
