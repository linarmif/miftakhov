﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace МакетКурсовойВП
{
    public partial class aVacancies : Form
    {
        public centrzEF db = new centrzEF();
        public List<vacancies> va;
        public aVacancies()
        {
            InitializeComponent();
            va = (from c in db.vacancies select c).ToList();
            var query = (from r in va
                         select new
                         {
                             r.num_vac,
                             r.num_employer,
                             r.specialty,
                             r.education,
                             r.tstazh,
                         }).ToList();
            dataGridView1.DataSource = query;
            dataGridView1.Columns[0].HeaderText = "Номер вакансии";
            dataGridView1.Columns[1].HeaderText = "Номер работодателя";
            dataGridView1.Columns[2].HeaderText = "Специальность";
            dataGridView1.Columns[3].HeaderText = "Образование";
            dataGridView1.Columns[4].HeaderText = "Стаж";
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            Close();
            aVacancies f3 = new aVacancies();
            f3.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                var rows = from o in db.vacancies
                           where o.num_vac.ToString() == textBox1.Text
                           select o;

                foreach (var row in rows)
                {
                    db.vacancies.Remove(row);
                }
                db.SaveChanges();
            }
            catch { MessageBox.Show("Ошибка"); }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Close();
        }   
    }
    
}
